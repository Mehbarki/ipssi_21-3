dcb: 
	docker-compose build
dcu: dcb
	docker-compose up -d
dcs: 
	docker-compose stop
dcsy: dcu
	sudo docker-compose exec php composer install 
	sudo docker-compose exec php php bin/console doctrine:schema:create
	sudo docker-compose exec php php bin/console doctrine:fixtures:load
	sudo docker-compose exec php php bin/console assets:install –symlink public/
